package de.unipassau.ep.boogle.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A NoSuchCommentException shows that a specified comment does not
 * exist in the database.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NoSuchCommentException extends RuntimeException {
    /**
     * Creates NoSuchComment exception with a message.
     * @param msg A {@link String} with the exception message
     */
    public NoSuchCommentException(final String msg) {
        super(msg);
    }
}
