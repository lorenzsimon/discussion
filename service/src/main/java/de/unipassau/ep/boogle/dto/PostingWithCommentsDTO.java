package de.unipassau.ep.boogle.dto;

import de.unipassau.ep.boogle.persistence.entities.Comment;
import de.unipassau.ep.boogle.persistence.entities.Posting;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * A DTO that functions as an output for a posting with all comments.
 */
@Data
public class PostingWithCommentsDTO {
    Posting posting;
    List<Comment> comments;

    /**
     * Default Constructor.
     */
    public PostingWithCommentsDTO() {
    }

    /**
     * Constructor of a {@link PostingWithCommentsDTO}
     * @param posting The {@link Posting}.
     * @param comments A {@link List} of all {@link Comment}s matching
     *                 the {@link Posting}.
     */
    public PostingWithCommentsDTO(Posting posting, List<Comment> comments) {
        this.posting = posting;
        this.comments = comments;
    }
}
