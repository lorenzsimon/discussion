package de.unipassau.ep.boogle.controller;

import de.unipassau.ep.boogle.dto.CommentDTO;
import de.unipassau.ep.boogle.dto.PostingDto;
import de.unipassau.ep.boogle.dto.PostingWithCommentsDTO;
import de.unipassau.ep.boogle.persistence.entities.Posting;
import de.unipassau.ep.boogle.service.DiscussionService;
import de.unipassau.ep.boogle.persistence.entities.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * The RestController for the discussion service.
 */
@RestController
public class DiscussionController {
    private final DiscussionService discussionService;

    /**
     * Constructor of the DiscussionController.
     * @param discussionService the {@link DiscussionService}.
     */
    @Autowired
    public DiscussionController(DiscussionService discussionService) {
        this.discussionService = discussionService;
    }

    /**
     * Tests the communication with the Discussion Service.
     * @param request A simple {@link String}.
     * @return A processed {@link String}.
     */
    @PostMapping("/discussion/test")
    public ResponseEntity<String> testRequest(@RequestBody String request) {
        return ResponseEntity.ok(discussionService.testRequest(request));
    }

    /**
     * Deletes a comment that is specified with the id.
     * @param cid The {@link Comment} id.
     * @return The {@link Comment} id.
     */
    @DeleteMapping("/discussion/comments/delete/{cid}")
    public ResponseEntity<String> deleteComment(@PathVariable String cid) {
        discussionService.deleteComment(cid);
        return new ResponseEntity<>(cid, HttpStatus.OK);
    }

    /**
     * Edits a comment that is specified with the id.
     * @param cid The {@link Comment} id.
     * @param editedComment A {@link String} containing the edited comment.
     * @return The {@link Comment} id.
     */
    @PostMapping("/discussion/comments/edit/{cid}")
    public ResponseEntity<String> editComment(@PathVariable String cid, @RequestBody String editedComment) {
        discussionService.editComment(cid, editedComment);
        return new ResponseEntity<>(cid, HttpStatus.OK);
    }

    /**
     * Adds a comment to the database.
     * @param commentDTO A {@link CommentDTO} containing the
     *                      comment content.
     * @return The {@link Comment} id.
     */
    @PostMapping("/discussion/comments/add")
    public ResponseEntity<String> addComment(@RequestBody CommentDTO commentDTO) {
        return new ResponseEntity<>(
                discussionService.addComment(commentDTO),
                HttpStatus.OK);
    }

    /**
     * Adds a new posting to the database.
     * It consists of a File (optional, not required) and a JSON object that
     * contains the user id and text.
     * @param file A {@link MultipartFile} (image or csv). This is optional and
     *             therefore not required.
     * @param postingDto A {@link PostingDto} that contains the user id and
     *                   the content ({@link String} of the posting.
     * @return The posting id as a {@link String} with {@code HttpStatus.OK},
     * otherwise {@code HttpStatus.BAD_REQUEST}.
     */
    @PostMapping("/discussion/posting/create")
    public ResponseEntity<String> createNewPosting(@RequestPart(value = "file", required = false) MultipartFile file,
                                                @RequestPart(value = "posting", required = true) PostingDto postingDto) {
        return new ResponseEntity<>(
                discussionService.storePosting(file, postingDto),
                HttpStatus.OK);
    }

    /**
     * Returns a Posting containing the content and the file, as well as all
     * matching comments.
     * @param postingid A {@link String} containing the posting id.
     * @return A {@link PostingWithCommentsDTO} containing the content, the
     * file data and the comments.
     * The file data can be {@code null} if there is no file.
     */
    @GetMapping("/discussion/posting/{postingid}")
    public ResponseEntity<PostingWithCommentsDTO> getPostingById(@PathVariable String postingid) {
        return new ResponseEntity<>(discussionService.getPostingById(postingid), HttpStatus.OK);
    }

    /**
     * Returns all postings with its comments that are found in the database.
     * @return A {@link List} of all {@link PostingWithCommentsDTO}s.
     */
    @GetMapping("/discussion/posting/all")
    public ResponseEntity<List<PostingWithCommentsDTO>> getAllPostings() {
        return new ResponseEntity<>(discussionService.getAllPostings(), HttpStatus.OK);
    }

    /**
     * Deletes a posting for the given id, as well as all comments the
     * reference the same posting.
     * @param postingid A {@link String} containing the posting id.
     * @return A {@link String} containing the id of the deleted posting.
     */
    @DeleteMapping("/discussion/posting/{postingid}")
    public ResponseEntity<String> deletePostingAndComments(@PathVariable String postingid) {
        return new ResponseEntity<>(
                discussionService.deletePostingAndComments(postingid),
                HttpStatus.OK);
    }

    /**
     * Deletes all Postings and Comments a user created, as well as the comments
     * matching the postings.
     * @param userid The user id.
     * @return A {@link String} containing the user id within a
     * {@code HttpStatus.OK}.
     */
    @DeleteMapping("discussion/user/{userid}")
    public ResponseEntity<String> deleteUserPostingsAndComments(@PathVariable String userid) {
        return new ResponseEntity<>(
                discussionService.deleteUserPostingsAndComments(userid),
                HttpStatus.OK);
    }
}
