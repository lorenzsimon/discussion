package de.unipassau.ep.boogle.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A FileStorageException shows that a file could not be stored in
 * the database.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FileStorageException extends RuntimeException {
    /**
     * Creates FileStorageException with a message.
     * @param message A {@link String} containing the error message.
     */
    public FileStorageException(String message) {
        super(message);
    }

    /**
     * Creates FileStorageException with a message.
     * @param message A {@link String} containing the error message.
     * @param cause The Throwable.
     */
    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
