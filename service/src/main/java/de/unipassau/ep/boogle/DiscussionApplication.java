package de.unipassau.ep.boogle;

import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

/**
 * The entrypoint for the Discussion-Service.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DiscussionApplication {

	/**
	 * Starts the application
	 * @param args the args are unused.
	 */
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	/**
	 * Declares the {@code LoggingService} bean.
	 * @return The {@code LoggingService} managed by spring.
	 */
	@Bean
	@Scope("singleton")
	public LoggingService loggingService() {
		return new LoggingService("discussion");
	}

}
