package de.unipassau.ep.boogle.repository;

import de.unipassau.ep.boogle.persistence.entities.Posting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This repository connects the service to the discussion database
 */
@Repository
public interface PostingRepository extends JpaRepository<Posting, String> {
    List<Posting> findAllByUserid(String userId);
}
