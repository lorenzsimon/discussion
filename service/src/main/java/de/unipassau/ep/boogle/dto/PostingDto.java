package de.unipassau.ep.boogle.dto;

import lombok.Data;

/**
 * A DTO that functions as an input for a posting.
 */
@Data
public class PostingDto {
    private String userId;
    private String content;
    private String title;

    /**
     * Constructor of the DTO.
     * @param userId A {@link String} containing the user id.
     * @param content A {@link String} containing the content of the posting.
     * @param title A {@link String} containing the title of the posting.
     */
    public PostingDto(String userId, String content, String title) {
        this.userId = userId;
        this.content = content;
        this.title = title;
    }
}
