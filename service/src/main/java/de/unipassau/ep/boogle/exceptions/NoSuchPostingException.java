package de.unipassau.ep.boogle.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A NoSuchPostingException shows that a specified posting does not
 * exist in the database.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class NoSuchPostingException extends IllegalArgumentException {
    /**
     * Creates NoSuchPostingException exception with a message.
     * @param message A {@link String} with the exception message
     */
    public NoSuchPostingException(String message) {
        super(message);
    }
}
