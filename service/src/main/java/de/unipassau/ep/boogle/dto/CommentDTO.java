package de.unipassau.ep.boogle.dto;

import lombok.Data;

/**
 * This class {@code DiscussionDTO} contains the necessary information
 * that is needed to save the comments of a certain user.
 */
@Data
public class CommentDTO {
    private String content;
    private String userId;
    private String postingId;

    /**
     * Constructor for a CommentDTO.
     * @param content A {@link String} containing the content of the comment.
     * @param userId A {@link String} containing the user id.
     * @param postingId A {@link String} containing the posting id.
     */
    public CommentDTO(String content, String userId, String postingId) {
        this.content = content;
        this.userId = userId;
        this.postingId = postingId;
    }
}
