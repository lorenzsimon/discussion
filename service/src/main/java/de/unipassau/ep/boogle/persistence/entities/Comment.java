package de.unipassau.ep.boogle.persistence.entities;

import de.unipassau.ep.boogle.dto.CommentDTO;
import de.unipassau.ep.boogle.exceptions.IllegalParameterException;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * This class is the entity of a comment in the Discussion Service.
 * The entities are saved in the database.
 */
@Data
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Column(length = 2000)
    private String content;
    @NotNull
    private Date date;
    @NotNull
    private String userId; // user id
    @NotNull
    private String postingId;

    /**
     * Default Constructor of a comment.
     */
    public Comment() {
    }

    /**
     * Constructor of a comment which sets values that are saved in the
     * database.
     * @param content The comment's content
     * @param date The comment's timestamp
     * @param userId The comment's user id
     * @param postingid The comment's query id
     */
    public Comment(String content, Date date, String userId, String postingid) {
        this.content = content;
        this.date = date;
        this.userId = userId;
        this.postingId = postingid;
    }

    /**
     * Constructor of a comment which sets the values that are saved in the
     * database if the {@link CommentDTO} contains values.
     * @param commentDTO A {@link CommentDTO} object containing the
     *                      comments content.
     * @throws IllegalParameterException Arguments are missing.
     */
    public Comment(CommentDTO commentDTO) throws IllegalParameterException {
        if(commentDTO.getContent() == null
                || commentDTO.getPostingId() == null
                || commentDTO.getUserId() == null) {
            throw new IllegalParameterException("One or more arguments are missing");
        }

        this.content = commentDTO.getContent();
        this.userId = commentDTO.getUserId();
        this.postingId = commentDTO.getPostingId();
        this.date = new Date();
    }
}
