package de.unipassau.ep.boogle.persistence.entities;

import de.unipassau.ep.boogle.dto.PostingDto;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Entity os  a posting that is saved in the database.
 * It contains an id, user id, content, date and image data.
 */
@Data
@Entity
public class Posting {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @NotNull
    private String userid;
    @NotNull
    @Column(length = 4000)
    private String content;
    @NotNull
    private String title;
    private Date date;
    private String imageName;
    private String imageType;
    @Lob
    private byte[] image; //size must be < 1 MByte


    /**
     * Default constructor of a posting.
     */
    public Posting() {

    }

    /**
     * Constructor of a posting.
     * @param userid The user id. Must not be {@code null}.
     * @param content The posting content. Must not be {@code null}.
     * @param title The title of the posting. Must not be {@code null}.
     * @param date The date of the posting.
     * @param imageName The name of the file.
     * @param imageType The type of the file.
     * @param image The base64 String containing the image data.
     */
    public Posting(@NotNull String userid, @NotNull String content,
                   @NotNull String title, Date date, String imageName,
                   String imageType, byte[] image) {
        this.userid = userid;
        this.content = content;
        this.title = title;
        this.date = date;
        this.imageName = imageName;
        this.imageType = imageType;
        this.image = image;
    }

    /**
     * Constructor of a posting.
     * @param postingDto A {@link PostingDto} containing the user id and
     *                   the content.
     */
    public Posting(PostingDto postingDto) {
        this.userid = postingDto.getUserId();
        this.content = postingDto.getContent();
        this.title = postingDto.getTitle();
        this.date = new Date();
    }
}
