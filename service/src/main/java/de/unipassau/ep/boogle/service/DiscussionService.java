package de.unipassau.ep.boogle.service;

import de.unipassau.ep.boogle.dto.CommentDTO;
import de.unipassau.ep.boogle.dto.PostingDto;
import de.unipassau.ep.boogle.dto.PostingWithCommentsDTO;
import de.unipassau.ep.boogle.exceptions.FileStorageException;
import de.unipassau.ep.boogle.exceptions.NoSuchCommentException;
import de.unipassau.ep.boogle.exceptions.NoSuchPostingException;
import de.unipassau.ep.boogle.persistence.entities.Comment;
import de.unipassau.ep.boogle.persistence.entities.Posting;
import de.unipassau.ep.boogle.repository.DiscussionRepository;
import de.unipassau.ep.boogle.repository.PostingRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * This service class handles the {@code Discussion}.
 */
@Service
public class DiscussionService {
    private final DiscussionRepository discussionRepository;
    private final PostingRepository postingRepository;

    /**
     * Constructor of the DiscussionService.
     * @param discussionRepository The {@link DiscussionRepository}.
     * @param postingRepository The {@link PostingRepository}.
     */
    @Autowired
    public DiscussionService(DiscussionRepository discussionRepository, PostingRepository postingRepository) {
        this.discussionRepository = discussionRepository;
        this.postingRepository = postingRepository;
    }

    /**
     * Used to test the communication with the Discussion Service.
     * @param request A simple {@link String}.
     * @return A {@link String} that adds "test: " in front of the request.
     */
    public String testRequest(String request) {
        return "test: " + request;
    }

    /**
     * Deletes a comment with the given comment id.
     * @param commentId The comment id.
     * @return The comment id.
     * @throws NoSuchCommentException No comment was found.
     */
    public String deleteComment(String commentId) throws NoSuchCommentException {
        Comment comment = discussionRepository.findCommentById(Integer.parseInt(commentId));
        if (comment == null) {
            throw new NoSuchCommentException("The given comment does not exist");
        }
        discussionRepository.delete(comment);
        return commentId;
    }

    /**
     * Edits a comment with the given comment id.
     * It also sets a new timestamp.
     * Throws a {@link NoSuchCommentException} if no comment was found for
     * the given id.
     * @param commentId The comment id.
     * @param editedComment The new content.
     * @return The comment id as a {@link String}.
     * @throws NoSuchCommentException No comment was found.
     */
    public String editComment(String commentId, String editedComment) throws NoSuchCommentException {
        Comment comment = discussionRepository.findCommentById(Integer.parseInt(commentId));
        if (comment == null) {
            throw new NoSuchCommentException("The given comment does not exist");
        }
        editedComment = parseJSON(editedComment, "content");
        comment.setContent(editedComment);
        comment.setDate(new Date());
        discussionRepository.save(comment);

        return commentId;
    }

    /**
     * Gets the value of the provided key from the given JSON String.
     * @param json The JSON {@link String} that is being parsed.
     * @param key The key of which the value should be returned.
     * @return The value of the given key if and only if the key exists,
     * otherwise {@code null}.
     */
    private String parseJSON(String json, String key) {
        JSONObject jsonObject = new JSONObject(json);
        Object object = jsonObject.get(key);
        if (object instanceof Boolean) {
            return "" + jsonObject.getBoolean(key);
        } else if (object instanceof String) {
            return jsonObject.getString(key);
        } else if (object instanceof Integer) {
            return "" + jsonObject.getInt(key);
        }
        return null;
    }

    /**
     * Adds a new comment to the database.
     * @param commentDTO A {@link CommentDTO} containing the
     *                      comments content.
     * @return The comment id as a {@link String}.
     */
    public String addComment(CommentDTO commentDTO) {
        Comment comment = new Comment(commentDTO);
        discussionRepository.save(comment);
        return "" + comment.getId();
    }


    /**
     * Stores a posting in the database.
     * Throws a {@link FileStorageException} if the file could not be saved in
     * the database.
     * @param file A {@link MultipartFile} containing the file data. Can also
     *             be null and will then not be saved.
     * @param postingDto A {@link PostingDto} containing the user id and
     *                   the content.
     * @return The posting id as a {@link String}.
     * @throws FileStorageException Invalid file name or file could not
     * be stored.
     */
    public String storePosting(MultipartFile file, PostingDto postingDto) throws FileStorageException {
        Posting posting = new Posting(postingDto);

        if (file != null) {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            try {
                if (fileName.contains("..")) {
                    throw new FileStorageException("Filename contains invalid path sequence!");
                }

                posting.setImageName(fileName);
                posting.setImageType(file.getContentType());
                posting.setImage(file.getBytes());
            } catch (IOException e) {
                throw new FileStorageException("Could not store file!", e);
            }
        }

        postingRepository.save(posting);

        return posting.getId();
    }

    /**
     * Returns a posting from the database.
     * Throws a {@link NoSuchPostingException} if no {@link Posting} was found
     * for the given id.
     * @param postingid The posting id.
     * @return A {@link PostingWithCommentsDTO} containing all saved data
     * (posting and its comments).
     * @throws NoSuchPostingException No posting was found.
     */
    public PostingWithCommentsDTO getPostingById(String postingid) throws NoSuchPostingException {
        PostingWithCommentsDTO pwc = new PostingWithCommentsDTO();
        pwc.setPosting(postingRepository.findById(postingid)
                .orElseThrow(() -> new NoSuchPostingException("Posting not found!")));

        pwc.setComments(discussionRepository.findCommentsByPostingId(postingid));

        return pwc;
    }

    /**
     * Returns a list of all postings.
     * @return a {@link List} containing all {@link PostingWithCommentsDTO}s.
     */
    public List<PostingWithCommentsDTO> getAllPostings() {
        List<PostingWithCommentsDTO> list = new ArrayList<>();
        List<Posting> postings = postingRepository.findAll();

        if (postings != null) {
            for (Posting posting : postings) {
                list.add(new PostingWithCommentsDTO(
                        posting,
                        discussionRepository.findCommentsByPostingId(posting.getId()))
                );
            }
        }

        return list;
    }

    // Compression - currently not in use
    private byte[] compressBytes(byte[] data) {
        System.out.println("compressBytes - original size: " + data.length);
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }

        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("compressBytes - compressed size: " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

    private byte[] decompressBytes(byte[] data) {
        System.out.println("decompressBytes - compressed size: " + data.length);
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];

        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataFormatException e) {
            e.printStackTrace();
        }

        System.out.println("decompressBytes - decompressed size: " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

    /**
     * Deletes a posting for the given posting id, as well as the matching
     * comments.
     * @param postingid The posting id as a {@link String}.
     * @return A {@link String} containing the posting id.
     * @throws NoSuchPostingException No posting was found.
     */
    public String deletePostingAndComments(String postingid) throws NoSuchPostingException {
        Posting posting = postingRepository.findById(postingid)
                .orElseThrow(() -> new NoSuchPostingException("Posting not found!"));

        postingRepository.delete(posting);

        List<Comment> comments = discussionRepository.findCommentsByPostingId(postingid);

        if (comments != null) {
            for (Comment comment : comments) {
                discussionRepository.delete(comment);
            }
        }
        return postingid;
    }

    /**
     * Deletes all Comments and Postings a user created.
     * Also deletes all comments for the postings.
     * @param userid A {@link String} containing the user id
     * @return The user id that has been deleted from the Discussion Service.
     */
    public String deleteUserPostingsAndComments(String userid) {
        List<Posting> userPostings = postingRepository.findAllByUserid(userid);
        List<Comment> userComments = discussionRepository.findAllByUserId(userid);

        if (userPostings != null) {
            for (Posting posting : userPostings) {
                deletePostingAndComments(posting.getId());
            }
        }

        if (userComments != null) {
            for (Comment comment : userComments) {
                discussionRepository.delete(comment);
            }
        }

        return userid;
    }

    /**
     * This gets called after the repository is initialized. It serves as the provider of the test data set, that is
     * also going to be persistent over restarts.
     */
    @PostConstruct
    private void addTestDataSet() {
        Posting posting = new Posting();
        posting.setUserid("7");
        posting.setTitle("ich bin ein titel");
        posting.setContent("ich bin ein posting");
        posting.setDate(new Date());
        postingRepository.save(posting);

        Comment comment1 = new Comment();
        comment1.setContent("Test Comment");
        comment1.setDate(new Date());
        comment1.setId(-1);
        comment1.setPostingId(posting.getId());
        comment1.setUserId("0");
        discussionRepository.save(comment1);

        Comment comment2 = new Comment();
        comment2.setContent("Test Comment");
        comment2.setDate(new Date());
        comment2.setId(-2);
        comment2.setPostingId(posting.getId());
        comment2.setUserId("0");
        discussionRepository.save(comment2);
    }
}
