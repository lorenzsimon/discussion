package de.unipassau.ep.boogle.repository;

import de.unipassau.ep.boogle.persistence.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * This repository connects the service to the discussion database (db-discussion)
 */
@Repository
public interface DiscussionRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findCommentsByPostingId(String postingId);
    Comment findCommentById(int id);
    List<Comment> findAllByUserId(String userId);
}
