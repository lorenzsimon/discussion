package de.unipassau.ep.boogle.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A IllegalParameterException shows that the parameters of a method
 * does not match the required fields.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class IllegalParameterException extends IllegalArgumentException {
    /**
     * Creates IllegalParameterException with a message.
     * @param msg A {@link String} with the exception message.
     */
    public IllegalParameterException(final String msg) {
        super(msg);
    }
}
