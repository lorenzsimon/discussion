# Documentation of the Discussion-Service:

## Main purpose of the Discussion-Service?

The Discussion-Service is used as a forum. Every logged in user can open up a new posting and add title, description and a picture. Other logged in users can then comment on these postings. Postings can be deleted from the user that opened it and from the admin. Comments can be edited or deleted form the user that wrote that comment. Admins can only delete comments but not edit them.

## Internal Service Logic:
The Discussion Service consists of two components, the MariaDB database and the Java Spring Application.\
The service depends on the Discovery Service as well as on the Discussion database.
Both have to be available in order to start the service and assure correct behaviour.

The Spring Application consists of these parts:
- Posting Class 
    - contains the title of the posting
    - the content of the posting
    - the possible pictures that can be added to postings
    - the id of the posting
    - the id of the user in order to know which user wrote the posting
- Comment Class 
    - contains the content of the comment (max length = 2000)
    - the id of the posting in order to have a reference to the posting the comment belongs to
    - the id of the comment itself
    - the id of the user in order to know which user wrote the comment
    - the date the comment was written or edited
- DiscussionService Class 
    - contains the interactions between the Controller and the table entries.
- DiscussionRepository Class
    - managing the CRUD operations for the comments
- PostingRepository Class
    - managing the CRUD operations for the postings
- RESTController
    - contains the mapping for the commmunication with the frontend as well as other backend services.

The MariaDB database consists of these parts:
A table (comment) to save instances of written comments.
It saves the following data for a comment:
- comment id
- posting id
- user id
- content of postings/comments
- date

A table (posting) to save instances of written posting.
It saves the following data for a posting:
- posting id
- user id
- content
- title
- date
- image data (name, type, data)

It will then be possible to edit these values via mappings in the RESTController.

More information about the values is in the Discussion-Service [Database Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/discussion/-/wikis/Discussion-Database).

## Currently Available Endpoints:

A full list of available endpoints is in the Discussion-Service [Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/discussion/-/wikis/Discussion-API).

### Error Responses

In the case of errors, the API responds with `4**` or `5**` HTTP codes. The response body contains information about the time, type, message and details of the error. The detail field is optional.

## Known Bugs

The PostConstruct adds duplicate entries on restart. Since the posting ID is an automatically and randomly generatred String it is not possible to check for the id on startup. Therefore a new posting with 2 comments is added to the database every time a restart occures.\
This could be solved by changing the id type to a number. This way `id = 0` could be used for dummy database values and also for a check if there already is an entry in the database.\
This was not implemented yet and is described in [Issue 13](ep/ws20_21/team_dis_1/discussion#13).\
NOTE: This bug does not have any influence on the correct behaviour of the forum, especially since service restarts are not supposed to happen often.